# Data Analysis

## Time Series

Learn how to work with Time Series data using Python

Lessons
-------

* [01 - Lesson - Single Exponential Smoothing](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/data-analysis/raw/master/lessons/Time-Series_Single-Exponential-Smoothing.ipynb)
* [02 - Lesson - Double Exponential Smoothing](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/data-analysis/raw/master/lessons/Time-Series_Double-Exponential-Smoothing.ipynb)
* [03 - Lesson - Triple Exponential Smoothing](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/data-analysis/raw/master/lessons/Time-Series_Triple-Exponential-Smoothing.ipynb)


## Linear Programming

Learn how to perform LP using Python

Lessons
-------

* [01 - Lesson - Linear Programming Using PuLP](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/data-analysis/raw/master/lessons/01_Linear-Programming_PuLP.ipynb)
* [02 - Lesson - Linear Programming Using PuLP](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/data-analysis/raw/master/lessons/02_Linear-Programming_PuLP.ipynb)
